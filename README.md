## express 프레임워크
express는 경량화 웹 개발 프레임 워크로, 노드를 이용한 웹 서비스나 웹 애플리케이션 개발에 가장 널리 쓰이는 확장 모듈 중 하나입니다. 대중적이고 인기가 가장 많죠. express는 기본 모듈 중 하나인 http 모듈을 이용하여 웹 서버를 구축하고 데이터를 표시하는 방식을 좀 더 추상화하여 웹 서비스나 웹 애플리케이션 개발을 더 편리하고 수월하게 할 수 있도록 다양한 API를 제공합니다.<br />
`npm i express`

## nodemon
서버 코드를 변경 할 때마다, 서버를 재시작하는게 꽤 귀찮지요? nodemon 이라는 도구를 사용하면 이를 자동으로 해줍니다.<br />
`npm i nodemon`

## morgan
로그 기록을 남겨주는 미들웨어<br />
`npm i morgan`

## jsonwebtoken
JWT 생성을도와주는 모듈<br />
`npm i jsonwebtoken`

## body-parser, cookie-parser
body-parser<br />
HTTPpost put 요청시 request body 에 들어오는 데이터값을 읽을 수 있는 구문으로 파싱함과 동시에 req.body 로 입력해주어 응답 과정에서 요청에 body 프로퍼티를 새로이 쓸 수 있게 해주는 미들웨어<br />
`npm i body-parser`

cookie-parser<br />
요청된 쿠키를 쉽게 추출할 수 있도록 도와주는 미들웨어 입니다. express의 request(req) 객체에 cookies 속성이 부여됩니다.<br />
`npm i cookie-parser`

## cors
express의 CORS 관련 헤더를 편하게 설정할 수 있는 미들웨어. <br />
`npm i cors`

## compression
Requests that pass through the middleware will be compressed. gzip<br />
`npm i compression`

## graphql
graphql: Facebook에서 만든 API 용 쿼리 언어 인 GraphQL에 대한 JavaScript 구현 모듈<br />
apollo-server-express: apollo-server-express이라는 미들웨어를 사용하면 Express.js에 쉽게 적용할 수 있습니다.<br />
graphql-fields: GraphQLResolveInfo를 요청 된 필드(객체 프라퍼티)의 맵으로 변환합니다. 모든 조각과 복제 된 필드를 깔끔한 개체로 평면화하여 모든 수준에서 요청 된 필드를 쉽게 확인할 수 있습니다<br />
graphql-tools: GraphQL 스키마를 설정할 때 도움을 주는 라이브러리<br />
graphql-depth-limit: GraphQL 깊이를 제한<br />
`npm i graphql apollo-server-express graphql-fields graphql-tools graphql-depth-limit`

## bcrypt
비밀번호 암호화 모듈!<br />
`npm i bcrypt`

## mongoose
몽구스를 express server에 연결<br />
`npm i mongoose`

### ■ `mongodb cloud url`
`https://cloud.mongodb.com/`<br />
로 접속

## openssl 인증서 생성(.react-ssr.com)!!
genrsa  -out `private.key` 2048 : private.key 생성(개인키 발급)<br />
rsa -in `private.key` -pubout -out `public.key` : private.key 기반 public.key 생성(공개키 발급)<br />
req -config ./openssl.cnf -new -key `private.key` -out `private.csr` : CSR( 인증요청서 ) 만들기<br />
genrsa -aes256 -out `rootCA.key` 2048 : rootCA.key 생성하기!<br />
req -config ./openssl.cnf -x509 -new -nodes -key `rootCA.key` -days 3650 -out `rootCA.pem` : rootCA 사설 CSR 생성하기<br />
x509 -req -in `private.csr` -CA `rootCA.pem` -CAkey `rootCA.key` -CAcreateserial -out `mycommonpem.crt` -days 3650 <br />
: 만들었던 private.csr을 나만의 커스텀 CA인 rootCA의 인증을 받아 mycommonpem.crt로 생성<br />
x509 -in `mycommonpem.crt` -out `mycommonpem.pem` -outform PEM : CRT( 인증서 ) pem 파일로! <br />
rsa -in `private.key` -out `private.pem` -outform PEM : 개인키 pem 파일로! <br />

## ApolloError case!!
`SyntaxError`: 신택스 에러, 문법에러!<br/>
`ValidationError`: validation이란 어떤 데이터의 값이 유효한지, 타당한지 확인하는 것<br/>
`AuthenticationError`: rest api 의 401 Unauthorized 인증에러!<br/>
`ForbiddenError`: rest api 의 403 Forbidden 접근 금지<br/>
`PersistedQueryNotFoundError`: ? ?<br/>
`PersistedQueryNotSupportedError`: ? ?<br/>
`UserInputError`: 유저입력에러!<br/>
`extends` ApolloError<br/>
<br/>
`ApolloError`: 위 에러의 부모class로 모든에러 생성 가능!

## graphql test url!!
https://localhost:5002/graphql<br />
https://lapi.react-ssr.com:5002/graphql



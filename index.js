import express from 'express';
import cors from 'cors';
import mongoose from 'mongoose';
import schema from './graphql/schema';
import { ApolloServer } from 'apollo-server-express';
import https from 'https';
import http from 'http';
import fs from 'fs';
import depthLimit from 'graphql-depth-limit';
import compression from 'compression';
import logger from 'morgan';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import Config from './lib/Config';
import * as Common from './lib/Common';

const app = express();
const port = 5002;
const uri = Config.mongodbUri;
const corsOptions = {
    origin: Config.origin,
    optionsSuccessStatus: 204,
    credentials: true,
}
//openSSL로 개인키, 인증서 생성해서 경로에 넣음!
const httpsOptions = {
    key: fs.readFileSync('./certificates/key.pem'),
    cert: fs.readFileSync('./certificates/cert.pem')
};
mongoose.Promise = global.Promise;
mongoose.connect(uri, { 
    keepAlive: true,
    useNewUrlParser: true, 
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false 
});
const server = new ApolloServer({
    schema,
    validationRules: [depthLimit(7)],
    formatError: Common.formatError,
    context: ({ req, res }) => {

        // http, https구분! boolen
        console.log('req.secure!!!!!!!!!!!', req.secure);

        return { req, res };
    },
});
// cors 설정
//app.use(cors());
app.use(cors(corsOptions));

//Requests that pass through the middleware will be compressed. gzip
app.use(compression());

// parse JSON and url-encoded query
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// parse cookie
app.use(cookieParser());

// print the request log on console
app.use(logger('dev'));

// set the secret key variable for jwt
app.set('jwt-secret', Config.secret);

server.applyMiddleware({ app, path: '/graphql', cors: false }); //cors: false로 expree cors 설정 사용!

//https 설정!!!
const httpsServer = https.createServer(httpsOptions, app);

httpsServer.listen(port, () => {
    console.log(`서버 실행!! 포트는? ${port}`);
});


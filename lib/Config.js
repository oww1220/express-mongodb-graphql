const dbName = 'test';
export default {
	secret: '14-i-ymhk^o4vgz^(lx@-n6e&x7*op)ia7b_3(2tnj5pp4nba%',
	mongodbUri: `mongodb+srv://testUser1:1234@clustertest.wm6zh.mongodb.net/${dbName}?retryWrites=true&w=majority`,
	origin: [
		'http://lwww.react-ssr.com:3000', 
		'http://lwww.react-ssr.com:3006', 
		'http://lwww.react-ssr.com:83',
		'https://lwww.react-ssr.com:3000', 
		'https://lwww.react-ssr.com:3006', 
		'https://lwww.react-ssr.com:83',
		'https://lwww.react-ssr.com'
	],
};

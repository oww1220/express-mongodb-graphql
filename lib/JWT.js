import jwt from 'jsonwebtoken';
import Config from '../lib/Config';

export const generateAccessToken = (user)=> {
	const promiseJWT = new Promise((resolve, reject) => {
		jwt.sign(
			{
				_id:user._id, 
				user_id:user.user_id, 
				user_name:user.user_name, 
				age:user.age, 
				gender: user.gender,
			},
			Config.secret,
			{
				//날짜는 @d 시간은 @h, 분은 @m 그냥 숫자만 넣으면 ms단위
				//expiresIn: '7d', 7일
				//expiresIn: '10h', 10시간
				//expiresIn: '60', 1분
				//expiresIn: 120, it will be expired after 120ms
				expiresIn: '15m',
				issuer: 'velopert.com',
				subject: 'userInfo'
			}, 
			(err, token) => {
				if (err) reject(err);
				resolve(token);
			}
		);
	});
	return promiseJWT;
};

//accesstoken check!
export const checkAccessToken = (req)=> {
	
	const auth = req.header('Authorization');

	console.log(auth);
	if(!auth) throw new Error('Authorization bearer required');
	
	const token = auth.split('Bearer ')[1];
	console.log('token', token);
	if(token === 'undefined') throw new Error('Authorization bearer required');
	
	return decodeJWT(token);
};

//generaterefreshToken!
export const generaterrefreshToken = (user)=> {
	const promiseJWT = new Promise((resolve, reject) => {
		jwt.sign(
			{
				user_id:user.user_id, 
				tokenVersion:user.tokenVersion,
			},
			Config.secret,
			{
				//날짜는 @d 시간은 @h, 분은 @m 그냥 숫자만 넣으면 ms단위
				//expiresIn: '7d', 7일
				//expiresIn: '10h', 10시간
				//expiresIn: '60', 1분
				//expiresIn: 120, it will be expired after 120ms
				expiresIn: '7d',
				issuer: 'velopert.com',
				subject: 'userInfo'
			}, 
			(err, token) => {
				if (err) reject(err);
				resolve(token);
			}
		);
	});
	return promiseJWT;
};

//decodeJWT check!
export const decodeJWT = (authorization)=> {
	const promiseJWT = new Promise((resolve, reject) => {
		jwt.verify(
			authorization,
			Config.secret,
			(err, decodedToken) => {
				if (err) reject(err);
				resolve(decodedToken);
			}
		);
	});
	return promiseJWT;
};


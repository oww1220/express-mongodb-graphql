//import crypto from 'crypto';
//import Config from '../lib/Config';
import bcrypt from 'bcrypt';

export const creatPasswordHash = (password)=> {
	const saltRounds = 10;
	const salt = bcrypt.genSaltSync(saltRounds);
	return bcrypt.hashSync(password, salt); 
};

export const comparePassword = (passwordOrigin, password)=> {
	return bcrypt.compareSync(password, passwordOrigin);
};


/*
export const createHmac = (password)=> {
	return crypto.createHmac('sha512', Config.secret).update(password).digest('base64');
};

export const checkPassword = (passwordOrigin, password)=> {
	const passwordInput = createHmac(password);
	return passwordOrigin === passwordInput;
};*/
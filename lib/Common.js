const immediately = 1 * 500;
const oneMinute = 1 * 60 * 1000;
const oneHour = 60 * 60 * 1000;
const oneDay = 60 * 60 * 1000 * 24;
const oneWeek = 60 * 60 * 1000 * 24 * 7;

//refreshTokken 쿠키생성 함수 
export const createRefreshTokenCookie = (res, refreshToken)=> {
  //refreshTokken 과 만료기간 맞추야됨!
  const expiresDay = refreshToken===''?new Date( Date.now() + (immediately)) : new Date( Date.now() + (oneWeek));
  const domainName = '.react-ssr.com';
  res.cookie('refreshToken', refreshToken, {
      expires: expiresDay,
      //secure: true,
      //sameSite: 'None',
      domain: domainName,
      httpOnly: true,
  });
};

// graphql error format!!
export const formatError = (err) => {
  console.error("------------------------( GraphQL Error start )------------------------");
  console.error("[Path]:", err.path);
  console.error("[Message]:", err.message);
  console.error("[Code]:", err.extensions.code);
  console.error("[Original Error]", err.originalError);
  console.error("------------------------( GraphQL Error end )------------------------");
  return err
}

// 유닉스 --> 시간변환
export const Unix_timestamp = (t) => {
  const date = new Date(t*1000);
  const year = date.getFullYear();
  const month = "0" + (date.getMonth()+1);
  const day = "0" + date.getDate();
  const hour = "0" + date.getHours();
  const minute = "0" + date.getMinutes();
  const second = "0" + date.getSeconds();
  return year + "/" + month.substr(-2) + "/" + day.substr(-2) + " " + hour.substr(-2) + ":" + minute.substr(-2) + ":" + second.substr(-2);
};

// 현재시간 --> 유닉스
export const Unix_timestampConv = ()=> {
    return Math.floor(new Date().getTime() / 1000);
};

import { makeExecutableSchema } from 'graphql-tools';
import UsersResolvers from './Users/resolver';
import UsersTypeDefs from './Users/type';

const schema = makeExecutableSchema({
    typeDefs:[UsersTypeDefs],
    resolvers:[UsersResolvers],
});

export default schema;
const typeDefs = `
    type User {
        _id: ID!
        user_id: String!
        user_name: String!
        age: Int!
        gender: String!
        password: String!
        admin: Boolean!
        couponCnt: Int!
        estimateSaveCnt: Int!
        tokenVersion: Int!
    }
    type UserTokken {
        foundUser: User!
        token: String!
    }

    type Query {
        getUser(user_id: String!, password: String!): UserTokken
        logOutUser: Boolean
        authUser(user_id: String!): User
        allUser: [User]
        hello: String
        resetToken(user_id: String!): String
        refreshAccessToken(refreshToken: String!): String
        refreshRefreshToken: String
    }

    input UserInput {
        user_id: String!
        user_name: String!
        age: Int!
        gender: String!
        password: String!
    }

    input UpdateInput {
        user_id: String
        user_name: String
        age: Int
        gender: String
    }

    type Mutation {
        createUser(input: UserInput): User
        updateUser(_id: ID!, input: UpdateInput): User
        deleteUser(_id: ID!): User
    }

`;

export default typeDefs;
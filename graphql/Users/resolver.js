import graphqlFields from 'graphql-fields';
import { ApolloError, AuthenticationError, UserInputError } from 'apollo-server-errors';
import User from '../../models/user';
import * as JWT from '../../lib/JWT';
import * as Crypto from '../../lib/Crypto';
import * as Common from '../../lib/Common';

const resolvers = {
    Query: {
        async allUser(root, {}, context, info) {
            const topLevelFields = graphqlFields(info);
            //console.log('root!!!',root, info);
            console.log('topLevelFields',topLevelFields);
            const allUser = await User.find();
            return allUser;
        },
        async resetToken(root, {user_id}, context, info) {
            //---start JWT 체크 로직
            const { req, res } = context;
            const foundUser = await User.findOne({ user_id });

            try{
                await JWT.checkAccessToken(req);

                /*check 완료시 로직및 리턴*/
                //DB에서 새로운 정보를 가지고 와서 토큰을 리셋시켜준다!!
                const token = await JWT.generateAccessToken(foundUser);
                //console.log('token', token);

                return token;
            }
            catch(err) {
                console.log('error', err.message);
                throw new AuthenticationError(err.message);
            }
            //end JWT 체크 로직---
        },
        async refreshRefreshToken(root, {}, context, info) {
            const { req, res } = context;

            if(req.cookies) {
                const { refreshToken } = req.cookies;
                //jwt 검증
                const decodeData = await JWT.decodeJWT(refreshToken);

                //검증데이터
                const { user_id, tokenVersion, exp } = decodeData;
                //console.log('decodeData', decodeData, tokenVersion, exp);

                //해당 유저 정보 불러옴!
                const foundUser = await User.findOne({ user_id });

                //유저 정보 다시검증
                if (!foundUser || foundUser.tokenVersion !== tokenVersion) {
                    console.log('토큰버전 체크!', foundUser.tokenVersion, tokenVersion, foundUser.tokenVersion !== tokenVersion);
                    throw new AuthenticationError('유저정보가 틀립니다!');
                }

                const { _id } = foundUser;
                const newTokenVersion = foundUser.tokenVersion + 1;

                //DB 업데이트!
                const updateUser =await User.findOneAndUpdate(
                    { _id}, 
                    Object.assign(foundUser, { tokenVersion: newTokenVersion }), 
                    { new: true }
                );
                console.log('updateUser!!!, RErefreshToken!!!', updateUser);

                //토큰 새로발행!
                const updateRefreshToken = await JWT.generaterrefreshToken(updateUser);
                
                //쿠키 새로구워줌! 
                Common.createRefreshTokenCookie(res, updateRefreshToken);
                    
                //acceessToken은 string으로 클라로 넘김! refreshToken 재발급인 경우!
                return await JWT.generateAccessToken(updateUser);
            }


        },
        async refreshAccessToken(root, {refreshToken}, context, info) {
            //---start JWT 체크 로직
            //refreshToken check!
            const { req, res } = context;

            console.log('req.headers!!!!!', req.headers, refreshToken);

            try{
                //jwt 검증
                const decodeData = await JWT.decodeJWT(refreshToken);

                //검증데이터
                const { user_id, tokenVersion, exp } = decodeData;
                //console.log('decodeData', decodeData, tokenVersion, exp);

                //해당 유저 정보 불러옴!
                const foundUser = await User.findOne({ user_id });

                //console.log('foundUser', foundUser);

                //유저 정보 다시검증
                if (!foundUser || foundUser.tokenVersion !== tokenVersion) {
                    console.log('토큰버전 체크!', foundUser.tokenVersion, tokenVersion, foundUser.tokenVersion !== tokenVersion);
                    throw new AuthenticationError('유저정보가 틀립니다!');
                }

                //refreshToken 만료기간이 24시간 이하면 refreshToken 재발급!
                const afterOneDay = Math.floor(Date.now() / 1000) + 60 * 60 * 24;
                console.log('현재 refreshToken exp:', Common.Unix_timestamp(exp), '만료기간:', Common.Unix_timestamp(afterOneDay), '만료 24시간 이하면 재발급==>', exp < afterOneDay);

                //리플레쉬 토큰이 만료시기가 되면 브라우져에게 알려줌!!
                if (exp < afterOneDay) {
                    return 'update';
                }

                /*check 완료시 로직및 리턴*/
                //acceessToken은 string으로 클라로 넘김! refreshToken 재발급 아닌 경우!
                return await JWT.generateAccessToken(foundUser);

            }
            catch(err) {
                console.log('error', err.message);
                throw new AuthenticationError(err.message);
                
            }

            //end JWT 체크 로직---
        },
        async authUser(root, {user_id}, context, info) {
            //---start JWT 체크 로직
            const { req, res } = context;
            try{
                await JWT.checkAccessToken(req);

                /*check 완료시 로직및 리턴*/
                const authUser = await User.findOne({ user_id });
                return authUser;
            }
            catch(err) {
                console.log('error', err.message);
                throw new AuthenticationError(err.message);
            }
            //end JWT 체크 로직---
        },
        async logOutUser(root, {}, context, info){

            //---start JWT 체크 로직
            const { req, res } = context;
            console.log('cookie delete!!!');
            //console.log(req.headers);
            // Clearing the cookie- 쿠키를 삭제해야되니 바로 만료되는 빈 값쿠키를 구워줌
            Common.createRefreshTokenCookie(res, '');
            
            return true;

        },
        async getUser(root, { user_id, password }, context, info){
            const { req, res, cookies } = context;
            const foundUser = await User.findOne({ user_id });

            //아이디가 없음
            if (!foundUser) {
                throw new UserInputError('ID is nothing');
            }
            else {
                //아이디는 있으나 패스워드(암호화된)가 틀림
                if(!Crypto.comparePassword(foundUser.password, password)) {
                    throw new UserInputError('passwrord is wrong');
                }
                //모두 일치!
                else {
                    const refreshToken = await JWT.generaterrefreshToken(foundUser);

                    //refreshToken 쿠키로 구워줌
                    Common.createRefreshTokenCookie(res, refreshToken);
                
                    //아이디와 패스워드가 맞으면 배리얼 토큰 발행
                    const token = await JWT.generateAccessToken(foundUser);
                    //console.log('token', token);
    
                    return {
                        foundUser,
                        token,
                    };
                }
                
            }
        },
    },
    Mutation: {
        async createUser(root, { input }, context, info) {
            const { user_id } = input;
            //console.log(user_id);
            const foundUser = await User.findOne({ user_id });
            //console.log(foundUser);
            if (foundUser) {
                throw new UserInputError('ID is already in use');
            }
            else {
                const encrypted = Crypto.creatPasswordHash(input.password);
                //console.log(encrypted);
                const data = {...input, password:encrypted}
                const createUser = await User.create(data);
                return createUser;
            }
            
        },
        async updateUser(root, { _id, input }, context, info) {
            //---start JWT 체크 로직
            const { req, res } = context;
            try{
                await JWT.checkAccessToken(req);

                /*check 완료시 로직및 리턴*/
                const updateUser = await User.findOneAndUpdate(
                    { _id}, 
                    input, 
                    { new: true }
                );

                return updateUser
            }
            catch(err) {
                //console.log('error', err.message);
                throw new AuthenticationError(err.message);
            }
            //end JWT 체크 로직---
            
        },
        async deleteUser(root, { _id }, context, info) {
            //---start JWT 체크 로직
            const { req, res } = context;
            try{
                await JWT.checkAccessToken(req);

                /*check 완료시 로직및 리턴*/
                return await User.findOneAndDelete({ _id });
            }
            catch(err) {
                console.log('error', err.message);
                throw new AuthenticationError(err.message);
            }
            //end JWT 체크 로직---
        },
    } 
}

export default resolvers;
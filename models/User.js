import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const UserSchema = new Schema({
    user_id: {
        type: String,
        required: true
    },
    user_name: {
        type: String,
        required: true
    },
    age: {
        type: Number
    },
    gender: {
        type: String
    },
    password: {
        type: String
    },
    admin: { 
        type: Boolean, 
        default: false 
    },
    couponCnt: {
        type: Number,
        default: 0,
    },
    estimateSaveCnt: {
        type: Number,
        default: 0,
    },
    tokenVersion: {
        type: Number,
        default: 0,
    }
});

export default mongoose.model('user', UserSchema);
